/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.pool;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author ANTON
 * http://howtodoinjava.com/core-java/multi-threading/java-thread-pool-executor-example/
 */
public class Task implements Runnable 
{
    private String name;
 
    public Task(String name) 
    {
        this.name = name;
    }
     
    public String getName() {
        return name;
    }
 
    @Override
    public void run() 
    {
        try
        {
            Long duration = (long) (Math.random() * 10);
            System.out.println("Doing a task during : " + name + " - duration: " + duration);
            TimeUnit.SECONDS.sleep(duration);
            System.out.println(Thread.currentThread().getName() + " - " + name + " - finishid at " + duration);
        } 
        catch (InterruptedException e) 
        {
            e.printStackTrace();
        }
    }
}
