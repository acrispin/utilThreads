/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author anton
 * https://examples.javacodegeeks.com/core-java/util/concurrent/executorservice/java-executorservice-example-tutorial/
 */
public class ExecutorServiceApp {

    private static Future task2 = null;
    private static Future task3 = null;

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        // execute the Runnable
        Runnable task1 = new MyRunnable("Task1", 2, 100);
        executor.execute(task1);
        
        for (int i = 0; i < 2; i++) {
            if ((task2 == null) || (task2.isDone()) || (task2.isCancelled())) {
                task2 = executor.submit(new MyRunnable("Task2", 4, 200));
            }

            if ((task3 == null) || (task3.isDone()) || (task3.isCancelled())) {
                task3 = executor.submit(new MyRunnable("Task3", 5, 100));
            }            
           
            if (task2.get() == null) {  // if null the task has finished
                System.out.println(i + 1 + ") Task2 terminated successfully");
            } else { // if it doesn't finished, cancel it                
                task2.cancel(true);
            }
            
            if (task3.get() == null) {
                System.out.println(i + 1 + ") Task3 terminated successfully");
            } else {
                task3.cancel(true);
            }
        }
        
        executor.shutdown();
        System.out.println("-----------------------");
        
        // wait until all tasks are finished
        executor.awaitTermination(1, TimeUnit.SECONDS);
        System.out.println("All tasks are finished!");

    }

}
