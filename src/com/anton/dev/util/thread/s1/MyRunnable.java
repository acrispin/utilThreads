/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s1;

/**
 *
 * @author anton
 * https://examples.javacodegeeks.com/core-java/util/concurrent/executorservice/java-executorservice-example-tutorial/
 */
public class MyRunnable implements Runnable {

    private final String name;
    private final int count;
    private final long timeSleep;

    MyRunnable(String name, int count, long timeSleep) {
        this.name = name;
        this.count = count;
        this.timeSleep = timeSleep;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int i = 1; i <= this.count; i++) {
            sum = sum + i;
        }
        System.out.println(name + " thread has sum = " + sum
                + " and is going to sleep for " + timeSleep);
        try {
            Thread.sleep(this.timeSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
