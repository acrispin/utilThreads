/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s4;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author anton
 * http://www.javaworld.com/article/2078809/java-concurrency/java-concurrency-java-101-the-next-generation-java-concurrency-without-the-pain-part-1.html
 */
public class ServerSocketThread {

    public static void main(String[] args) throws IOException {
        /**
            Para comunicarse desde terminal con telnet, ejm
            $ telnet 127.0.0.1 9000
            o
            $ telnet localhost 9000
        */
        ServerSocket socket = new ServerSocket(9000);
        while (true) {
            final Socket s = socket.accept();
            Runnable runner = new Runnable() {
                @Override
                public void run() {
                    UtilSocket.doWork5(s);
                }
            };
            new Thread(runner).start();
        }
    }
}
