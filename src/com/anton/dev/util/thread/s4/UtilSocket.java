/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author anton
 */
public class UtilSocket {
    

    public static void doWork1(Socket s) {
        System.out.println("Running socket.");
        System.out.println("HostName client: " + s.getInetAddress().getHostName());
        System.out.println("HostAddress client: " + s.getInetAddress().getHostAddress());
        System.out.println("Port client: " + s.getPort());
        System.out.println("HostName local: " + s.getLocalAddress().getHostName());
        System.out.println("HostAddress local: " + s.getLocalAddress().getHostAddress());
        System.out.println("Port client: " + s.getLocalPort());
        try {
            System.out.println("OutputStream: " + s.getOutputStream().toString());
            System.out.println("InputStream: " + s.getInputStream().toString());
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        System.out.println("");
    }

    // http://stackoverflow.com/questions/5680259/using-sockets-to-send-and-receive-data
    public static void doWork2(Socket s) {
        DataInputStream dIn;
        try {
            dIn = new DataInputStream(s.getInputStream());
            System.out.println("Message1 : " + dIn.readByte());
            System.out.println("Message2 : " + dIn.readUTF());
            dIn.close();
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        System.out.println("");
    }

    // http://www.journaldev.com/741/java-socket-programming
    // No funciona desde terminal con telnet: $ telnet 127.0.0.1 9000
    // sale error ejm: Error: invalid stream header: 73737373
    public static void doWork3(Socket s) {
        try {
            ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
            String message = (String) ois.readObject();
            System.out.println("Message Received: " + message);

            ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
            oos.writeObject("Hi Client ");

            ois.close();
            oos.close();
            s.close();

        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        System.out.println("");
    }

    // http://stackoverflow.com/questions/13894509/java-telnet-socket-bufferedreader-bufferedwriter
    public static void doWork4(Socket s) {
        BufferedReader bwin = null;
        try {
            bwin = new BufferedReader(new InputStreamReader(s.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            while (true) {
                String readFir = bwin.readLine();
                if (readFir == null) {
                    break;
                }
                System.out.println(readFir);
                if (readFir.startsWith("Please")) {
                    System.out.println("Password Entered");
                    bw.write("abc123");
                    bw.newLine();
                    // instead might have to explicitly write "\r\n"
                    // depending platform you're connecting from.
                    bw.flush();
                } else if (readFir.startsWith("Enter")) {
                    System.out.println("Enter command");
                    bw.write("log set-info 1");
                    bw.newLine();
                    bw.flush();
                    bw.close();  //close buffered Writer
                    break;
                } else {
                    System.out.println("Skip: " + readFir);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        } finally {
            try {
                bwin.close();
            } catch (IOException ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        }
    }

    // http://stackoverflow.com/questions/3763511/sending-telnet-commands-and-reading-the-response-with-java
    public static void doWork5(Socket s) {
        try {
            PrintWriter out = null;
            BufferedReader in = null;
            out = new PrintWriter(s.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            s.close();
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
}
