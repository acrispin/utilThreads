/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s5;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author anton
 * http://javarevisited.blogspot.pe/2012/05/how-to-use-threadlocal-in-java-benefits.html?m=1
 */
public class ThreadLocalTest {

    public static void main(String args[]) throws IOException, InterruptedException {
        System.out.println(new java.rmi.dgc.VMID().toString());
        
        Thread t1 = new Thread(new Task());   
        Thread t2 = new Thread( new Task());
      
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
        
        if (true) {
            return;
        }
        
        for (int i = 0; ; i++) {
            // don't subclass Thread.
            new Thread() {
                // this is somewhat pointless as you are defining a ThreadLocal per thread.
                final ThreadLocal<Object> tlObject = new ThreadLocal<Object>() {
                };

                public void run() {
                    tlObject.set(new byte[8 * 1024 * 1024]);
                }
            }.start();
            Thread.sleep(1);
            if (i % 1000 == 0) {
                System.gc();
                System.out.println(i);
            }
            
            if (i == 10000*10) {
                break;
            }
        }
      
    }
    
    /*
     * Thread safe format method because every thread will use its own DateFormat
     */
    public static String threadSafeFormat(Date date) {
        DateFormat formatter = PerThreadFormatter.getDateFormatter();
        //PerThreadFormatter.remove();
        return formatter.format(date);
    }
    
}


/*
 * Thread Safe implementation of SimpleDateFormat
 * Each Thread will get its own instance of SimpleDateFormat which will not be shared between other threads. *
 */
class PerThreadFormatter {

    private static final ThreadLocal<SimpleDateFormat> dateFormatHolder = new ThreadLocal<SimpleDateFormat>() {

        /*
         * initialValue() is called
         */
        @Override
        protected SimpleDateFormat initialValue() {
            System.out.println("Creating SimpleDateFormat for Thread : " + Thread.currentThread().getName());
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };

    /*
     * Every time there is a call for DateFormat, ThreadLocal will return calling
     * Thread's copy of SimpleDateFormat
     */
    public static DateFormat getDateFormatter() {
        return dateFormatHolder.get();
    }
    
    // http://stackoverflow.com/questions/12424838/threadlocal-remove
    public static void remove() {
        dateFormatHolder.remove();
    }
}

class Task implements Runnable {
    
    @Override
    public void run() {
        for(int i=0; i<2; i++) {
            System.out.println("Thread: " + Thread.currentThread().getName() + " Formatted Date: " + ThreadLocalTest.threadSafeFormat(new Date()) );
        }
    }
}
