/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread.s2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author anton
 * http://www.journaldev.com/1069/threadpoolexecutor-java-thread-pool-example-executorservice
 */
public class SimpleThreadPoolApp {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            Runnable worker = new WorkerThread("" + i);
            executor.execute(worker);
        }
        System.out.println("Shutdown executor");
        executor.shutdown();
        
        // bloquea por el tiempo que se le pasa como parametro hasta que todas las tareas son ejecutadas
        // devuelve true si se cumplio las tareas en el tiempo especificado
        // se llama despues de un shutdown()
        if (executor.awaitTermination(1, java.util.concurrent.TimeUnit.SECONDS)) {
            System.out.println("Tareas cumplieron con el tiempo especificado");
        } else {
            System.out.println("No se cumplio con el tiempo especificado");
        }
        
        // bloquea hasta que todas las tareas fueron ejecutadas
        while (!executor.isTerminated()) {
        }
        
        System.out.println("Finished all threads");
    }
}
