/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.thread;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * ExecutorService java
 *
 * @author anton
 */
public class App {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        testScheduleExecutorService();
    }

    public static void testCountProcessors() {
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("cores: " + cores);
    }
    
    public static void testScheduleExecutorService() throws InterruptedException {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        // ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
        ScheduledFuture<?> handler = scheduler.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    int i = ThreadLocalRandom.current().nextInt(1, 10 + 1);
                    System.out.println("Ejecucion de tare..... " + i + " date: " + new Date());
                    if (i % 3 == 0) {
                        System.out.println("Comenzando sleep de " + i +" segundos.");
                        try {
                            Thread.sleep(1000*6);
                        } catch (InterruptedException ex) {
                            System.out.println("Error:: " + ex);
                        }
                    }
                }
            }, 0, 2, TimeUnit.SECONDS);
        
        // 
        Thread.sleep(1000*20);
        System.out.println("Cancenlando todas las tareas");
        scheduler.shutdownNow();
        scheduler = null;
        // handler.cancel(true);
        // handler = null;
    }

    /**
     * execute(Runnable)
     * http://tutorials.jenkov.com/java-util-concurrent/executorservice.html#executorservice-usage
     */
    public static void testExecuteRunnable() {
        ExecutorService exeService = Executors.newFixedThreadPool(10);
        exeService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("Tarea asyncrona ...");
            }
        });
        exeService.shutdown();
    }

    /**
     * submit(Runnable)
     * http://tutorials.jenkov.com/java-util-concurrent/executorservice.html#executorservice-usage
     */
    public static void testSubmitRunnable() {
        try {
            ExecutorService exeService = Executors.newFixedThreadPool(10);
            Future future = exeService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3 * 1000);
                        if (true) {
                            throw new RuntimeException("Error forzado");
                        }
                        System.out.println("Tarea asyncrona future...");
                    } catch (InterruptedException | RuntimeException ex) {
                        System.out.println("Error2: " + ex.getMessage());
                    }
                }
            });
            Object obj = future.get(); //returns null if the task has finished correctly.
            if (obj == null) {
                System.out.println("OK");
            } else {
                System.out.println("NOT OK");
            }
            exeService.shutdown();
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    /**
     * submit(Callable)
     * http://tutorials.jenkov.com/java-util-concurrent/executorservice.html#executorservice-usage
     * Cause warning: uses unchecked or unsafe operations.
     */
    public static void testSubmitCallable() {
        try {
            ExecutorService exeService = Executors.newFixedThreadPool(10);
            Future future = exeService.submit(new Callable() {
                @Override
                public Object call() {
                    System.out.println("Tarea asyncrona future callable...");
                    return "Resultado de llamada";
                }
            });
            System.out.println("result future: " + future.get());
            exeService.shutdown();
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    /**
     * invokeAny()
     * http://tutorials.jenkov.com/java-util-concurrent/executorservice.html#executorservice-usage
     */
    public static void testinvokeAny() {
        try {
            ExecutorService executorService = Executors.newSingleThreadExecutor();

            Set<Callable<String>> callables = new HashSet<>();

            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 1";
                }
            });
            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 2";
                }
            });
            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 3";
                }
            });

            String result = executorService.invokeAny(callables);

            System.out.println("result = " + result);

            executorService.shutdown();
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    /**
     * invokeAll()
     * http://tutorials.jenkov.com/java-util-concurrent/executorservice.html#executorservice-usage
     */
    public static void invokeAll() {
        try {
            ExecutorService executorService = Executors.newSingleThreadExecutor();

            Set<Callable<String>> callables = new HashSet<>();

            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 1";
                }
            });
            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 2";
                }
            });
            callables.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return "Task 3";
                }
            });

            List<Future<String>> futures = executorService.invokeAll(callables);

            for (Future<String> future : futures) {
                System.out.println("future.get = " + future.get());
            }

            executorService.shutdown();
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

}
