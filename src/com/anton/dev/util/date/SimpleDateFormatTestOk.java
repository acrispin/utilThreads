/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author ANTON
 * http://stackoverflow.com/questions/4021151/java-dateformat-is-not-threadsafe-what-does-this-leads-to/4021932#4021932
 * http://fahdshariff.blogspot.pe/2010/08/dateformat-with-multiple-threads.html
 */
public class SimpleDateFormatTestOk {
    private static final ThreadLocal<DateFormat> DFT = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyyMMdd");
        }
    };
    public static void main(String[] args) throws Exception {

        final ThreadLocal<DateFormat> format = new ThreadLocal<DateFormat>() {
            @Override
            protected DateFormat initialValue() {
                System.out.println("INIT SimpleDateFormat - " + Thread.currentThread().getName());
                return new SimpleDateFormat("yyyyMMdd");
            }
        };

        Callable<Date> task = new Callable<Date>(){
            @Override
            public Date call() throws Exception {
                System.out.println("Parse - " + Thread.currentThread().getName());
                return format.get().parse("20101022");
            }
        };

        //pool with 5 threads
        ExecutorService exec = Executors.newFixedThreadPool(5);
        List<Future<Date>> results = new ArrayList<>();
        
        // http://stackoverflow.com/questions/22795563/how-to-use-callable-with-void-return-type/22795897#22795897
        Callable<Void> task2 = new Callable<Void>(){
            @Override
            public Void call() {
                System.out.println("task2 - " + Thread.currentThread().getName());
                return null;
            }
        };
        Future<Void> future2 = exec.submit(task2);
        future2.get(); // wait for completion

        //perform 20 date conversions
        for(int i = 0 ; i < 20 ; i++) {
            results.add(exec.submit(task));
        }
        exec.shutdown();

        //look at the results
        for(Future<Date> result : results) {
            System.out.println(result.get());
        }
        
        format.remove(); // TODO: Investigar si siempre es necesario llamar a este metodo
    }
}
