/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.fact;

import java.util.concurrent.Callable; 
import java.util.concurrent.ExecutionException; 
import java.util.concurrent.ExecutorService; 
import java.util.concurrent.Executors; 
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;


/**
 *
 * @author ANTON
 */
public class FactorialTest {
    public static void main(String... args) throws InterruptedException, ExecutionException { 
        // creating thread pool to execute task which implements Callable 
        //ExecutorService es = Executors.newSingleThreadExecutor();
        //ExecutorService es = Executors.newFixedThreadPool(4);
        //ExecutorService es = Executors.newCachedThreadPool();
        
        //ExecutorService es = Executors.newSingleThreadExecutor(new CustomThreadFactory());
        //ExecutorService es = Executors.newFixedThreadPool(4, new CustomThreadFactory());
        ExecutorService es = Executors.newCachedThreadPool(new CustomThreadFactory());
        
        System.out.println("submitted callable task to calculate factorial of 10"); 
        Future<Long> result10 = es.submit(new FactorialCalculator(10)); 
        System.out.println("submitted callable task to calculate factorial of 15"); 
        Future<Long> result15 = es.submit(new FactorialCalculator(15)); 
        System.out.println("submitted callable task to calculate factorial of 20"); 
        Future<Long> result20 = es.submit(new FactorialCalculator(20)); 
        System.out.println("submitted callable task to calculate factorial of 25"); 
        Future<Long> result25 = es.submit(new FactorialCalculator(25)); 
        
        System.out.println("Calling get method of Future to get result of factorial 10"); 
        long factorialof10 = result10.get(); 
        System.out.println("factorial of 10 is : " + factorialof10); 
        System.out.println("Calling get method of Future to get result of factorial 15"); 
        long factorialof15 = result15.get(); 
        System.out.println("factorial of 15 is : " + factorialof15); 
        System.out.println("Calling get method of Future to get result of factorial 20"); 
        long factorialof20 = result20.get(); 
        System.out.println("factorial of 20 is : " + factorialof20);
        System.out.println("Calling get method of Future to get result of factorial 25"); 
        long factorialof25 = result25.get(); 
        System.out.println("factorial of 25 is : " + factorialof25);
        
        es.shutdown();
        
        System.out.println("isDaemon father: " + Thread.currentThread().isDaemon());
        Thread t1 =  new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Other thread child.......");
            }
        });
        t1.start();
        System.out.println("isDaemon child: " + t1.isDaemon());        
    }
}

class FactorialCalculator implements Callable<Long> { 
    private final int number; 
    public FactorialCalculator(int number){ 
        this.number = number; 
    } 
    @Override 
    public Long call() throws Exception { 
        return factorial(number); 
    } 

    private long factorial(int n) throws InterruptedException { 
        long result = 1; 
        while (n != 0) { 
            result = n * result; 
            n = n - 1; 
            Thread.sleep(100); 
        } 
        return result; 
    } 
}

// http://stackoverflow.com/questions/6113746/naming-threads-and-thread-pools-of-executorservice/6113794#6113794
class CustomThreadFactory implements ThreadFactory {
    
    @Override
    public Thread newThread(Runnable r) {
        System.out.println("INIT Thread " + Thread.currentThread().getName());
        return new Thread(r);
    }
    
 }
