/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.socket.s1;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author anton
 * http://www.binarytides.com/java-socket-programming-tutorial/
 * compilar y ejecutar en terminal: 
 * $ javac SocketServerMultConn.java && java SocketServerMultConn 
 * ejecutar comando en otra terminal: 
 * $ telnet localhost 5000
 */
public class SocketServerMultConn {

    public static void main(String args[]) {
        ServerSocket s = null;
        Socket conn = null;

        try {
            //1. creating a server socket - 1st parameter is port number and 2nd is the backlog
            s = new ServerSocket(5000, 10);

            //2. Wait for an incoming connection
            echo("Server socket created. Waiting for connection...");

            while (true) {
                //get the connection socket
                conn = s.accept();

                //print the hostname and port number of the connection
                echo("Connection received from " + conn.getInetAddress().getHostName() + " : " + conn.getPort());

                //create new thread to handle client
                new ClientHandler(conn).start();
            }
        } catch (IOException e) {
            System.err.println("IOException");
        }

        //5. close the connections and stream
        try {
            s.close();
        } catch (IOException ioException) {
            System.err.println("Unable to close. IOexception");
        }
    }

    public static void echo(String msg) {
        System.out.println(msg);
    }
}

class ClientHandler extends Thread {

    private Socket conn;

    ClientHandler(Socket conn) {
        this.conn = conn;
    }

    @Override
    public void run() {
        String line, input = "";

        try {
            //get socket writing and reading streams
            DataInputStream in = new DataInputStream(conn.getInputStream());
            PrintStream out = new PrintStream(conn.getOutputStream());

            //Send welcome message to client
            out.println("Welcome to the Server");

            //Now start reading input from client
            while ((line = in.readLine()) != null && !line.equals(".")) {
                //reply with the same message, adding some text
                out.println("I got : " + line);
                System.out.println(String.format("cliente %d: %s", conn.getPort(), line));
            }

            //client disconnected, so close socket
            conn.close();
        } catch (IOException e) {
            System.out.println("IOException on socket : " + e);
            e.printStackTrace();
        }
    }
}
