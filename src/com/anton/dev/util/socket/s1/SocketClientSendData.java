/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.socket.s1;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author anton
 * http://www.binarytides.com/java-socket-programming-tutorial/
 * compilar y ejecutar en terminal
 * $ javac SocketClientSendData.java && java SocketClientSendData
 */
public class SocketClientSendData {

    public static void main(String[] args) throws IOException {
        Socket s = new Socket();
        String host = "www.google.com";
        PrintWriter s_out = null;

        try {
            s.connect(new InetSocketAddress(host, 80));
            System.out.println("Connected");

            //writer for socket
            s_out = new PrintWriter(s.getOutputStream(), true);
        } //Host not found
        catch (UnknownHostException e) {
            System.err.println("Don't know about host : " + host);
            System.exit(1);
        }

        //Send message to server
        String message = "GET / HTTP/1.1\r\n\r\n";
        s_out.println(message);

        System.out.println("Message send");
    }
}
