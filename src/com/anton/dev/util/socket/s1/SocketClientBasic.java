/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anton.dev.util.socket.s1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author anton
 * http://www.binarytides.com/java-socket-programming-tutorial/
 * compilar y ejecutar en terminal
 * $ javac SocketClientBasic.java && java SocketClientBasic
 */
public class SocketClientBasic {

    public static void main(String[] args) throws IOException {
        Socket s = new Socket();
        String host = "www.google.com";

        try {
            s.connect(new InetSocketAddress(host, 80));
        }
        catch (UnknownHostException e) {
            System.err.println("Don't know about host : " + host);
            System.exit(1);
        }

        System.out.println("Connected");
    }
}
